@extends('layouts.master')


 @section('content')
     <div class="container-fluid ">
         <div class="row">
             <div class="col-md-5" style="">
                 <div class="row">
                     <div class="col-sm-1-12">
                         <input class="form-control" type="search" name="searchWord" id="search-word" placeholder="ИСКАТЬ">
                     </div>
                 </div>
                 <div class="row">
                     <div class="col-sm-1-12">
                         <h3>Filtr-tag-cloud</h3>
                     </div>
                 </div>
                 <div class="row">
                     <div class="col-sm-1-12">
                        <h3>Alphabet</h3>
                     </div>
                 </div>
             </div>
             <div class="col-md-7 bg-sand px-4">
                 <div class="row">
                     <h1 class="display-5">Русско-лакский словарь</h1>
                 </div>
                 <div class="row">
                     <table class="table" id="front_list">
                         <tr>
                             <td><img class="preview rounded py-3" src="{{asset('img/tiger.jpg')}}" alt=""></td>
                             <td>Тигр</td>
                             <td>Циникь</td>
                             <td><i class="fa fa-volume-up " aria-hidden="true"></i></td>
                         </tr>
                         <tr>
                             <td><img class="preview rounded py-3" src="{{asset('img/cat.jpg')}}" alt=""></td>
                             <td>Кошка</td>
                             <td>Ччиту</td>
                             <td><i class="fa fa-volume-up" aria-hidden="true"></i></td>
                         </tr>
                         <tr>
                             <td><img class="preview rounded py-3" src="{{asset('img/dog.jpg')}}" alt=""></td>
                             <td>Собака</td>
                             <td>Ккаччи</td>
                             <td><i class="fa fa-volume-up" aria-hidden="true"></i></td>
                         </tr>
                     </table>
                 </div>
             </div>
         </div>
     </div>
 @stop